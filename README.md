# ArcDps Updater

Automatically updates ArcDps and different addons when an update is available.

## Installation

### [Download](https://apps.reyko.online/arc_updater/ArcDps%20Updater.exe)

1. Start "Arc Updater" for first time to generate the Config file.

2. Once generated it will open it for set up. 

You can insert the gamepath, if not "Arc Updater" will scan for it
on next start.

## Contributing

Any suggestions or constructive criticism are welcome.

## Disclaimer

I am not affiliated or partnered with ArenaNet or the ArcDps (& addons) projects.
